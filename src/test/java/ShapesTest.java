/*
 * The Shapes.java class should use the Writer to print the area of the given shape using the dimensions
 * given in the parameters. Every time the Shapes.java.area method is invoked, it should add a new line.
 *
 * Please implement this class to have all tests in ShapesTest turn green.
 * Try doing that with the simplest, cleanest code you can craft.
 *
 * Setup the classpath and make sure you can run the ShapesTest.
 * The ShapesTest results will be a main factor when evaluating the solution.
 */

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShapesTest {

    private StringWriter out;
    private Shapes shapes;

    @BeforeEach
    public void initWriter() {
        out = new StringWriter();
        shapes = new Shapes(out);
    }

    @Test
    public void square() throws IOException {
        shapes.area("SQUARE", "30");
        assertEquals("900\n", out.toString());
    }

    @Test
    public void rectangle() throws IOException {
        shapes.area("RECTANGLE", "50,20");
        assertEquals("1000\n", out.toString());
    }

    @Test
    public void triangle() throws IOException {
        shapes.area("TRIANGLE", "50,20");
        assertEquals("500\n", out.toString());
    }

    @Test
    public void squareAndTriangle() throws IOException {
        shapes.area("SQUARE", "30");
        shapes.area("TRIANGLE", "40,20");
        assertEquals("900\n400\n", out.toString());
    }

    @Test
    public void degradedTriangle() throws IOException {
        shapes.area("TRIANGLE", "20");
        assertEquals("200\n", out.toString());
    }

}